import React from 'react';
import { useState } from 'react';

export default function Interval() {

    const [times, setTimes] = useState([]);

    function addTime()
    {
        var time = Date.now();
        setTimes([...times, convertTime(time)]);
    }

    function convertTime(time)
    {
        // convert time to format HH:MM:SS-millis
    
        var date = new Date(time);

        // add leading zero if needed
        var hours = (date.getHours() < 10 ? '0' : '') + date.getHours();
        var minutes = (date.getMinutes() < 10 ? '0' : '') + date.getMinutes();
        var seconds = (date.getSeconds() < 10 ? '0' : '') + date.getSeconds();
        var milliseconds = date.getMilliseconds();
        
        return hours + ':' + minutes + ':' + seconds + '-' + milliseconds;
    }

    return (
    <>
    <center>
        <h1>W41</h1>
        <button style={{fontSize: '36px'}} onClick={() => {
            addTime();
        }}>Interval time</button>

        {times.map((time, index) => {
            return <p key={index}>{time}</p>
        })}
    </center>
    </>
)};