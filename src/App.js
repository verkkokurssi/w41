import React from 'react';
import logo from './logo.svg';
import './App.css';
import Interval from './Interval';

function App() {
  return (
    <Interval/>
  );
}

export default App;
